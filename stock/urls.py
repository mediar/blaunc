from django.conf.urls import url, include
from . import views
from rest_framework import routers




router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)


app_name = 'stock'

urlpatterns = [

    #############DATA-URLS#######

    url(r'^api/chart/consum_data_mth$',views.ConsumDataMonths.as_view(),name='consum-data-month'),
    url(r'^api/chart/consum_data_evt/(?P<string>[\w\-]+)$',views.ConsumDataEvents.as_view(),name='consum-data-event'),




    ###########END DATA-URLS#######


    ######## REST_FRAMEWORK #########

    url(r'^api-auth/', include('rest_framework.urls',namespace='rest_framework')),
    url(r'^rest_api/',include(router.urls)),
    url(r'^rest_event_user/(?P<string>[\w\-]+)/',views.EventRESTView.as_view(),name='event_restview'),



    ##### END REST_FRAMEWORK ########





    url(r'^$',views.StocksOverview.as_view(),name='index'),


    url(r'^stockoverview/',views.StocksOverview.as_view(),name='stockoverview'),
    url(r'^stock/add$',views.stock_create,name='stock-create'),
    url(r'^(?P<pk>[0-9]+)/$',views.StockDetailView.as_view(),name='details'),
    url(r'^stock/updt$',views.StockUpdate.as_view(),name='stock-update'),
    url(r'^stock/(?P<pk>[0-9]+)/delete/$',views.StockDelete.as_view(),name='stock-delete'),


    #find stock url by ID



    #Create new Stock/Item

    url(r'^item/add/(?P<pk>[0-9]+)$',views.item_create,name='item-create'),
    url(r'^item/(?P<pk>[0-9]+)$',views.ItemUpdate.as_view(),name='item-update'),
    url(r'^item/(?P<pk>[0-9]+)/delete/$',views.ItemDelete.as_view(),name='item-delete'),




    url(r'^storeoverview/$',views.store_overview,name='storeoverview'),


    #Logistik
    url(r'^storelogistic/fill/(?P<pk>[0-9]+)/(?P<pk2>[0-9]+)$',views.FillItem.as_view(),name='item-fill'),
    url(r'^storelogistic/(?P<pk>[0-9]+)/$',views.StoreLogistic.as_view(),name='store-log'),
    url(r'^storelogistic/(?P<pk>[0-9]+)/(?P<pk2>[0-9]+)/$',views.store_detail_log,name='store-detail-log'),
    url(r'^storelogistic/fill/(?P<pk>[0-9]+)/(?P<pk2>[0-9]+)/(?P<string>[\w\-]+)/$',views.fill_obj_fill,name='fill-obj-fill'),
    url(r'^storelogistic/(?P<pk>[0-9]+)/delete$',views.fill_man_delete,name='fill_man-delete'),


    url(r'^store/(?P<pk>[0-9]+)/delete/$',views.StoreDelete.as_view(),name='store-delete'),
    url(r'^store/create$',views.store_create,name='store-create'),
    url(r'^store/(?P<pk>[0-9]+)$',views.StoreDetail.as_view(),name='store-detail'),
    url(r'^store/reset/(?P<pk>[0-9]+)$',views.store_reset,name='reset-store'),



    url(r'^store/fill/storemanager/(?P<pk>[0-9]+)$',views.add_item_to_store, name='additemtostore'),
    url(r'^store/add/$',views.storeManager_create ,name='storemanager-add'),


    url(r'^menu/$',views.MenuOverview.as_view(),name='menu-overview'),
    url(r'^menu/add/$',views.menu_create,name='menu-create'),
    url(r'^menu/(?P<pk>[0-9]+)/$',views.MenuDetailView.as_view(),name='menu-detail'),
    url(r'^menu/storeitem/add$',views.storeItem_create,name='storeitem-create'),
    url(r'^menu/storeitem/delete/(?P<pk>[0-9]+)/$',views.delete_menu_item,name='delete-menu-item'),

    #Update Stock/Item

    #DeleteView   :TODO:Delete Confirmation

    #Bestellung

    #url(r'^stock/(?P<pk>[0-9]+)$',views.OrderView.as_view(),name='order'),
    #url(r'^ordermanager/add$',views.OrderCreate.as_view(),name='ordermanager'),
    #url(r'^order/(?P<pk>[0-9]+)$',views.passOrderView.as_view(),name='passorder'),


    #Lieferanten
    url(r'^supplier/add$',views.SupplierCreate.as_view(),name='supplier-add'),
    url(r'^supplieroverview/$',views.SupplierOverview.as_view(),name='supplieroverview'),



    #Calender und Events
    url(r'^calendar/$',views.calendar,name='calendar'),
    url(r'^event/add/$',views.event_create,name='event-create'),
    url(r'^event/detail/(?P<pk>[0-9]+)$',views.event_detail,name='event-detail'),
    url(r'^event/begin/(?P<pk>[0-9]+)$',views.event_begin,name='event-begin'),
    url(r'^event/end/(?P<pk>[0-9]+)$',views.event_end,name='event-end'),
    url(r'^event/(?P<pk>[0-9]+)/delete/$',views.event_delete,name='event-delete'),

    #url(r'^role/add/(?P<pk>[0-9]+)/(?P<pk2>[0-9]+)$',views.role_create,name='role-create'),


    url(r'^role/add/(?P<pk>[0-9]+)$',views.role_create,name='role-create'),
    url(r'^role/(?P<pk>[0-9]+)/delete/$',views.role_delete,name='role-delete'),




    #Verzehr(consum)

    url(r'^consum/(?P<pk>[0-9]+)/(?P<string>[\w\-]+)$',views.consum_storeitem_by_type,name='consum-storeitem-by-type'),
    url(r'^consum/store/(?P<pk>[0-9]+)/$',views.consum_thk,name='consum-store'),
    url(r'^consum/(?P<pk>[0-9]+)/$',views.consum_item,name='consum-item'),
    url(r'^add_item_to_consum/$',views.add_item_to_consum,name='add-item-to-consum'),

   ### Lieferung (delivery)
   url(r'^delivery/(?P<pk>[0-9]+)/$',views.delivery_enter,name='delivery-enter'),
   url(r'^delivery/save/(?P<pk>[0-9]+)/$',views.delivery_save,name='delivery-save'),

   ###### Journal ######
   url(r'^journal/$',views.journal_overview,name='journal'),


   ########Export#######

   url(r'^export/(?P<pk>[0-9]+)/$',views.export_to_xls,name='export-to-xls'),




]

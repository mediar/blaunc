from django.contrib.auth.models import User
from django import forms
from django.forms import formset_factory
from .models import Item, Stock, Delivery, DeliveryManager, Supplier, Store, StoreManager,FillManager, UserProfile,Event, RoleManager,Menu, StoreItem
from django.core.files.images import get_image_dimensions
from django.contrib.admin.widgets import AdminSplitDateTime, AdminDateWidget
from django.contrib.admin import widgets

import datetime
from django.forms.extras.widgets import SelectDateWidget



class ItemForm(forms.ModelForm):


    class Meta:
        model = Item
        exclude = ('stock','to_order','filled')

class FillStoreForm(forms.ModelForm):

    class Meta:
        model = StoreManager
        fields = ['on_store']

#TODO: create fillform:

class FillForm(forms.ModelForm):
    #unit = forms.IntegerField(widget=forms.NumberInput(attrs={'autofocus':'autofocus'}),null=True)
    class Meta:
        model=FillManager
        fields = ['container','unit']





class StockForm(forms.ModelForm):

    class Meta:
        model = Stock
        fields = ['name']

class SupplierForm(forms.ModelForm):



    class Meta:
        model = Supplier
        fields = ['name','phone_number','email']



class UserForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput)
    email = forms.CharField(widget=forms.EmailInput)


    class Meta:
        model = User
        fields = ['username' , 'email', 'password',]




class StoreForm(forms.ModelForm):

    class Meta:
        model = Store
        fields = ['name' , 'stock','color']

class StoreManagerForm(forms.ModelForm):

    class Meta:
        model = StoreManager
        fields = ['item','on_store','store','name']



class EventForm(forms.ModelForm):
    date= forms.DateField(widget=SelectDateWidget())
    #date_end = forms.DateField(widget=SelectDateWidget())
    class Meta:

        model = Event
        exclude=('begin','end','total_personal_consum')


class MenuForm(forms.ModelForm):

    class Meta:
        model=Menu
        exclude=()

class StoreItemForm(forms.ModelForm):

    class Meta:
        model=StoreItem
        exclude=()

class RoleForm(forms.ModelForm):


    class Meta:
        model=RoleManager
        exclude=('event',)


    def clean(self):

        role = self.cleaned_data.get('role')
        store=self.cleaned_data.get('store')

        if role == 'THK' and not store:
            msg=forms.ValidationError("bitte Theke hinzufuegen")
            self.add_error('store', msg)

        return self.cleaned_data

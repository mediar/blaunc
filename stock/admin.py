from django.contrib import admin
from .models import Stock, Item,Delivery, DeliveryManager, Supplier, Store, StoreManager,FillObject, FillManager, UserProfile, Event, Consum, consumManager, RoleManager, StoreItem, Menu,EventLog









# Register your models here.
###admin interface for "Stock"
admin.site.register(Stock)
admin.site.register(Item)
admin.site.register(Delivery)
admin.site.register(DeliveryManager)
admin.site.register(Supplier)
admin.site.register(StoreManager)
admin.site.register(Store)

admin.site.register(FillObject)
admin.site.register(FillManager)

admin.site.register(RoleManager)

admin.site.register(UserProfile)
admin.site.register(Event)
admin.site.register(EventLog)


admin.site.register(Consum)
admin.site.register(consumManager)


admin.site.register(Menu)
admin.site.register(StoreItem)

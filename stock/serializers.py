from django.contrib.auth.models import User, Group
from .models import Event, Consum
from rest_framework import serializers, viewsets
from django.core.exceptions import ValidationError


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username',)

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = '__all__'

class ConsumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consum
        fields = '__all__'

from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.http import Http404
from django.template.loader import render_to_string
from .models import Stock, Item, Supplier, Store, StoreManager, FillManager, Event, Consum,consumManager, RoleManager, Menu, StoreItem, FillObject, EventLog, Delivery, DeliveryManager, JournalEntry
from django.shortcuts import render, get_object_or_404,redirect, render_to_response
from django.contrib.auth import authenticate, login
from django.views import generic
from django.views.generic import View, TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .forms import UserForm, ItemForm, StockForm, SupplierForm, StoreForm, StoreManagerForm, FillStoreForm, FillForm, EventForm, RoleForm, MenuForm, StoreItemForm
import json, xlwt
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User, Group





from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_framework import viewsets
from .serializers import UserSerializer, GroupSerializer, EventSerializer, ConsumSerializer

import decimal






from django.utils.decorators import method_decorator
from django.core.exceptions import PermissionDenied

from django.forms import modelformset_factory, inlineformset_factory, formset_factory

from datetime import date


##########REST_FRAMEWORK############

class UserViewSet(viewsets.ModelViewSet):

    queryset = User.objects.all()
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):

    queryset = Group.objects.all()
    serializer_class = GroupSerializer



##########END REST_FRAMEWORK##########


def export_to_xls(request,pk):
    stock=Stock.objects.get(id=pk)
    file_name="inventur-"+stock.name
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="'+file_name+'.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('inventur')


    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Artikel', 'Bestand', ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = stock.item_set.all().values_list('name', 'on_stock')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response





def group_required(*group_names):
    """Requires user membership in at least one of the groups passed in."""
    def in_groups(u):
        if u.is_authenticated():
            if bool(u.groups.filter(name__in=group_names)) | u.is_superuser:
                return True
            else:
                raise PermissionDenied
    return user_passes_test(in_groups)






# Create your views here.


class IndexView(generic.ListView):
    template_name='stock/index.html'
    context_object_name = 'all_stocks'
    def get_queryset(self):
        return Stock.objects.all()


class StockDetailView(generic.DetailView):
    model = Stock
    template_name='stock/stockdetail.html'










def stock_detail(request,stock_id):

    stock = get_object_or_404(Stock, pk=stock_id)

    return render(request,'stock/detail.html',{'stock' : stock})








@method_decorator(group_required('admins'),name='dispatch')
class StocksOverview(TemplateView):


    template_name='stock/stockoverview.html'
    def get_context_data(self, **kwargs):
        context = super(StocksOverview, self).get_context_data(**kwargs)
        context['all_stocks'] = Stock.objects.all()
        return context


    def dispatch(self,request,*args,**kwargs):
        return super(StocksOverview,self).dispatch(request,*args,**kwargs)

class StoreLogistic(TemplateView):


    template_name='stock/logistics_templates/store_log.html'
    def get_context_data(self, **kwargs):
        context = super(StoreLogistic, self).get_context_data(**kwargs)
        fill_obj=FillObject.objects.get(id=self.kwargs['pk'])
        context
        context['all_stores'] = Store.objects.all()
        context['fill_obj'] = fill_obj
        return context

class StoreDelete(DeleteView):
    model = Store
    success_url = reverse_lazy('stock:storeoverview')


def store_overview(request):

    template_name='stock/storeoverview.html'
    context={}
    context['all_stores'] = Store.objects.all()
    return render(request,template_name,context)




class StoreOverview(View):


    template_name='stock/storeoverview.html'
    def get_context_data(self, **kwargs):
        context = super(StoreOverview, self).get_context_data(**kwargs)
        context['all_stores'] = Store.objects.all()
        return render(template_name, context)




@group_required('admins')
def store_create(request):
    data=dict()
    if request.method == 'POST':
        form = StoreForm(request.POST)
        if form.is_valid():
            form.save()
            data['form_is_valid']=True

        else :
            data['form_is_valid']=False
    else :
        form = StoreForm()

    context = {'form': form }
    data['html_form']=render_to_string('stock/store_form.html',context,request=request)
    return JsonResponse(data)

def save_storemanager_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)




@group_required('admins')
def add_item_to_store(request,pk):
    data=dict()
    storeman=StoreManager.objects.get(id=pk)
    if request.method == 'POST':
        form = FillStoreForm(request.POST,instance=storeman)
        #if form.is_valid():
        #    form.save()
        #    data['form_is_valid']=True


        #else :
        #    data['form_is_valid']=False
    else :
        form = FillStoreForm(instance=storeman)
    return save_storemanager_form(request, form, 'stock/additemtostore_form.html')
    #context = {'form': form }
    #data['html_form']=render_to_string('stock/additemtostore_form.html',context,request=request)
    #return JsonResponse(data)








def save_storemanager_formset(request, formset, template_name):
    data = dict()
    if request.method == 'POST':
        if formset.is_valid():
            formset.save()
            data['formset_is_valid'] = True
        else:
            data['formset_is_valid'] = False
    context = {'formset': formset}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


#TODO create fill view

#class AddItemToStore(generic.UpdateView):
#    model = StoreManager
#    form_class = FillStoreForm
#    template_name = 'stock/additemtostore_form.html'
#    def dispatch(self, *args,**kwargs):
#        self.storemanager_id=kwargs['pk']
#        return super(AddItemToStore,self).dispatch(*args,**kwargs)
#
#    def form_valid(self,form):
#        form.save()
#        storeman = StoreManager.objects.get(id=self.storemanager_id)
#        return HttpResponse (render_to_string('stock:store-fill',{'storemanager': storeman} ))








class FillItem(generic.CreateView):
    model = FillManager
    form_class= FillForm
    def form_valid(self, form):

        store_man = StoreManager.objects.get(id=self.kwargs['pk'])
        form.instance.store = store_man.store
        form.instance.item = store_man.item
        form.instance.name = form.instance.item.name
        form.instance.to_fill=form.instance.container
        form.instance.fillobject = FillObject.objects.get(id=self.kwargs['pk2'])
        return super(FillItem, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(FillItem, self).get_context_data(**kwargs)
        fill_obj=FillObject.objects.get(id=self.kwargs['pk2'])
        context
        context['fill_obj'] = fill_obj
        return context

def fill_man_delete(request,pk):
    fill_man=FillManager.objects.get(id=pk)
    fill_man.delete()
    context={}
    context['store']=fill_man.store
    context['fill_obj']=fill_man.fillobject
    return render(request,"stock/logistics_templates/store_log_detail_by_type.html", context)



def fill_obj_fill(request,pk,pk2,string):
    store_obj=Store.objects.get(id=pk)
    fill_obj = FillObject.objects.get(id=pk2)

    items_by_type=store_obj.items.filter(type=string)
    context={}
    context['items']=items_by_type
    context['type']=string
    context['store']=store_obj
    context['fill_obj']=fill_obj
    return render(request,"stock/logistics_templates/store_log_detail_items.html",context)


def store_detail_log(request,pk,pk2):
    store=Store.objects.get(id=pk)
    fill_obj=FillObject.objects.get(id=pk2)
    context={}
    context['store']=store
    context['fill_obj']=fill_obj
    return render(request,"stock/logistics_templates/store_log_detail_by_type.html", context)



class StoreDetail(generic.DetailView):
    model = Store
    template_name = "stock/store_detail.html"

class StoreFill(generic.DetailView):
    model = Store
    template_name = "stock/store_fill.html"

#class storeManager_create(generic.CreateView):
#    model = StoreManager
#    fields = ['item','on_store','store','name',]

def store_reset(request,pk):
    store=Store.objects.get(id=pk)
    for store_man in store.storemanager_set.all():
        store_man.on_store=0.0
        store_man.save()
    return render(request,"stock/store_detail.html",{'store':store})


def storeManager_create(request):


    data=dict()
    if request.method == 'POST':
        form = StoreManagerForm(request.POST)
        if form.is_valid():
            form.save()
            data['form_is_valid']=True
        else :
            data['form_is_valid']=False
    else :
        form = StoreManagerForm()


    context = {'form': form }
    data['html_form']=render_to_string('stock/storemanager_form.html',context,request=request)
    return JsonResponse(data)








class SupplierOverview(TemplateView):
    template_name='stock/supplieroverview.html'
    def get_context_data(self, **kwargs):
        context = super(SupplierOverview,self).get_context_data(**kwargs)
        context['all_supplier'] = Supplier.objects.all()
        return context




class SupplierCreate(View):
    form_class=SupplierForm
    template_name='stock/supplier_form.html'
    def post(self,request):
        form=self.form_class(request.POST)
        if form.is_valid():
            supplier = form.save(commit=False)
            supplier.save()
            return redirect('stock:supplieroverview')
        else:
            return render(request,self.template_name,{'form':form})



    def get(self,request):
        form = self.form_class(None)
        return render(request,self.template_name,{'form':form})






def save_stock_form(request, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
        else:
            data['form_is_valid'] = False
    context = {'form': form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)




def stock_create(request):
    if request.method == 'POST':
        form = StockForm(request.POST)
    else:
        form = StockForm()
    return save_stock_form(request, form, 'stock/stock_form.html')


class StockCreate(View):
    form_class=StockForm
    template_name='stock/stock_form.html'
    def post(self,request):
        form=self.form_class(request.POST)
        if form.is_valid():
            stock = form.save(commit=False)
            stock.save()
            return redirect('stock:stockoverview')

    def get(self,request):
        form = self.form_class(None)
        return render(request,self.template_name,{'form':form})

def save_item_form(request,pk, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            instance=form.save(commit=False)
            instance.stock=Stock.objects.get(id=pk)
            instance.save()
            data['form_is_valid'] = True
        else:
            data['form_is_valid'] = False
    context = {'form': form, 'id':pk}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)





def item_create(request,pk):

    if request.method == 'POST':
        form = ItemForm(request.POST)
    else :
        form = ItemForm()
    return save_item_form(request,pk,form, 'stock/item_form_create.html')

class ItemCreate(View):
    form_class=ItemForm
    template_name='stock/item_form.html'
    def post(self,request,pk):
        form=self.form_class(request.POST)
        stock=Stock.objects.get(id=pk)
        if form.is_valid():
            item = form.save(commit=False)
            item.stock=stock
            item.save()
            return redirect('stock:details', item.stock.pk)



    def get(self,request,pk):
        form = self.form_class(None)
        return render(request,self.template_name,{'form':form})

class ItemUpdate(generic.UpdateView):
    model = Item
    fields = ['name','on_stock','unit_over','on_stock_target','on_stock_min','price','item_per_st','type','size']





class StockUpdate(generic.UpdateView):
    model = Stock
    fields = ['name']


class StockDelete(generic.DeleteView):
    model = Stock
    success_url = reverse_lazy('stock:stockoverview')







class ItemDelete(generic.DeleteView):
    model = Item
    success_url = reverse_lazy('stock:stockoverview')



class UserFormView(View):
    form_class = UserForm
    template_name = 'stock/registrationform.html'

    def get(self,request):
        form = self.form_class(None)
        return render(request,self.template_name,{'form':form})

    def post(self,request):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save(commit=False)

            #Cleaned (Normalized) Data
            username=form.cleaned_data['username']
            password=form.cleaned_data['password']
            user.set_password(password)
            user.save()


            user = authenticate(username=username,password=password)

            if user is not None:

                if user.is_active:
                    login(request,user)
                    return redirect('home')

        return render(request,self.template_name,{'form':form})




class Calendar(View):
    template_name = 'stock/calendar_templates/calendar.html'
    def get(self,request):
        return render(request,self.template_name)



def calendar(request):

    all_event = Event.objects.all()
    context = {"Schicht":all_event}
    return render(request, 'stock/calendar_templates/calendar.html',context)

class EventCreate(CreateView):
    model = Event
    fields=['name','start_date','end_date','user','color']

def event_create(request):
    if request.method == 'POST':
        form = EventForm(request.POST)
    else:
        form = EventForm()
    return save_stock_form(request, form, 'stock/event_templates/event_form.html')


def save_role_form(request,pk, form, template_name):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            instance=form.save(commit=False)
            instance.event=Event.objects.get(id=pk)
            instance.save()
            data['form_is_valid'] = True
        else:
            data['form_is_valid'] = False
    context = {'form': form, 'id':pk}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


def role_create(request,pk):

    if request.method == 'POST':
        form = RoleForm(request.POST)
    else:
        form = RoleForm()
    return save_role_form(request,pk,form, 'stock/role_form.html')




def event_detail(request,pk):
    event=get_object_or_404(Event,id=pk)
    all_stores=Store.objects.all()
    users=User.objects.exclude(username='admin')
    fillobject=FillManager.objects.all()
    return render(request,'stock/event_templates/event_detail.html',{'event':event,'all_stores':all_stores,'users':users,'fillobject':fillobject,})


def log_detail(request):
    all_fill_man = FillManager.objects.all()
    context = {"all_fill_man":all_fill_man}
    return render(request, 'stock/log_detail.html',context)



def consum_storeitem_by_type(request,pk,string):
    storeitem_by_type=StoreItem.objects.filter(type=string)
    consum=Consum.objects.get(id=pk)
    return render(request, 'stock/store_templates/storeitem_by_type.html',{'storeitems':storeitem_by_type,'consum':consum})

def consum_thk(request,pk):
    template_name="stock/store_templates/consum_store.html"
    event=Event.objects.get(id=pk)
    store_user = request.user
    consum_list=[]
    for consum in Consum.objects.all():
        if consum.event == event:
            consum_list.append(consum)

    return render(request,template_name,{'consum_list':consum_list, 'event':event})

def consum_item(request,pk):
    template_name="stock/store_templates/consum_item.html"
    menu=Menu.objects.get(name="Personal")
    consum=Consum.objects.get(id=pk)
    return render(request,template_name,{'consum':consum,'menu':menu})

def add_item_to_consum(request):
    value=request.POST.get("value")
    consum_id=request.POST.get("consum")
    item=StoreItem.objects.get(id=str(value))
    consum=Consum.objects.get(id=str(consum_id))
    for obj in consum.consummanager_set.all():
        if obj.storeitem==item:
            obj.increment_consum(1)
            obj.save()
            if consum.user.username=="Perso_privat":
                consum.increment_sum(item.price/2)
            else:
                consum.increment_sum(item.price)
            consum.save()


    resp={}
    return HttpResponse(resp,content_type="application/json")


def event_begin(request,pk):
    event=Event.objects.get(id=pk)
    menu=Menu.objects.get(name="Personal")
    if request.method == 'POST':
        event.begin=True
        for user in event.rolemanager_set.all():
            if user.role == "CVD":
                pass

            elif user.role == "LOG" or user.role=="CVD_LOG":
                cns=Consum.objects.create(user=user.user,role=user.role,event=event)
                for item in menu.storeitem_set.all():
                    consumManager.objects.create(consum=cns,storeitem=item)
                fill_obj = FillObject.objects.create(user=user.user,event=event)

            else:
                cns=Consum.objects.create(user=user.user,role=user.role,event=event)
                for item in menu.storeitem_set.all():
                    consumManager.objects.create(consum=cns,storeitem=item)

        for user in User.objects.all():
            if Consum.objects.filter(user=user,event=event).exists():
                pass
            else:
                if user.groups.filter(name__in=['CHEF']).exists():
                    cns=Consum.objects.create(user=user,event=event)
                    for item in menu.storeitem_set.all():
                        consumManager.objects.create(consum=cns,storeitem=item)
                elif user.groups.filter(name__in=['PRIV']).exists():
                    cns=Consum.objects.create(user=user,event=event)
                    for item in menu.storeitem_set.all():
                        consumManager.objects.create(consum=cns,storeitem=item)
                elif user.groups.filter(name__in=['SECURITY']).exists():
                    cns=Consum.objects.create(user=user,event=event)
                    for item in menu.storeitem_set.all():
                        consumManager.objects.create(consum=cns,storeitem=item)




        event.save()

    return redirect('stock:event-detail',event.id)





def event_end(request,pk):
    event=Event.objects.get(id=pk)
    if request.method == 'POST':
        event.end=True
        event.calc_total_personal_sum
        event.save()
        for fill_obj in event.fillobject_set.all():
            for fill_man in fill_obj.fillmanager_set.all():
                event_log,created=EventLog.objects.get_or_create(event=event,item=fill_man.item)
                if created:
                    event_log.c_filled=fill_man.container
                    event_log.u_filled=fill_man.unit
                    event_log.save()
                else:
                    event_log.c_filled=event_log.c_filled+fill_man.container
                    event_log.u_filled=event_log.u_filled+fill_man.unit
                    event_log.save()
        for event_log in event.eventlog_set.all():
            event_log.norm_unit_to_container()
            event_log.save()
            event_log.item.substractOnStockChange(event_log.c_filled,event_log.u_filled)
            event_log.calc_sales()
            event_log.save()


    return redirect('home')

def event_delete(request,pk):
    event=Event.objects.get(id=pk)
    event.delete()
    return redirect('stock:calendar')


@method_decorator(group_required('admins'),name='dispatch')
class MenuOverview(TemplateView):


    template_name='stock/menuoverview.html'
    def get_context_data(self, **kwargs):
        context = super(MenuOverview, self).get_context_data(**kwargs)
        context['all_menus'] = Menu.objects.all()
        return context


    def dispatch(self,request,*args,**kwargs):
        return super(MenuOverview,self).dispatch(request,*args,**kwargs)



def menu_create(request):
    if request.method == 'POST':
        form = MenuForm(request.POST)
    else:
        form = MenuForm()
    return save_stock_form(request, form, 'stock/menu_form.html')

class MenuDetailView(generic.DetailView):
    model = Menu
    template_name='stock/menu_detail.html'

def storeItem_create(request):
    if request.method == 'POST':
        form = StoreItemForm(request.POST)
    else:
        form = StoreItemForm()
    return save_stock_form(request, form, 'stock/storeItem_form.html')


def delete_menu_item(request,pk):
    item=StoreItem.objects.get(id=pk)
    menu = item.getMenu()
    item.delete()
    return redirect('stock:menu-detail',menu.id)



def role_delete(request,pk):
    role=RoleManager.objects.get(id=pk)
    event=role.event
    role.delete()
    return redirect('stock:event-detail',event.id)



def delivery_enter(request,pk):

    """
    delivered = request.POST.getlist('delivered')
    items_delivered = Item.objects.filter(id__in=delivered)
    """
    stock=Stock.objects.get(id=pk)


    return render(request,'stock/delivery.html',{'stock':stock})


def delivery_save(request,pk):

    items_delivered=request.POST.getlist('item')
    stock=Stock.objects.get(id=pk)
    delivery = Delivery.objects.create(stock=stock)

    for it in items_delivered:
        if request.POST['delivery-{}'.format(it)] !=0:
            item=Item.objects.get(id=int(it))
            item.on_stock=int(item.on_stock)+int(request.POST['delivery-{}'.format(it)])
            item.save()
            delivery_manager = DeliveryManager.objects.create(delivery=delivery,item=item,delivered=int(request.POST['delivery-{}'.format(it)]))

    return redirect('stock:details',stock.id)

def journal_overview(request):
    context={}
    all_entries=JournalEntry.objects.all()

    context['all_entries']=all_entries
    return render(request,'stock/journal_templates/journal.html',context)







#################DATA-VIEWS#########

class ConsumDataMonths(APIView):
    authentication_classes=[]
    permission_classes = [AllowAny]
    def get(self,request,format=None):
        months = ["01","02","03","04","05","06","07","08","09","10","11","12"]
        consum = []
        for month in months:
            ev_month=Event.objects.filter(date__month=month)
            consum_mth = 0
            for evt in ev_month:
                consum_mth=consum_mth+evt.total_personal_consum
            consum.append(consum_mth)

        data ={
                'labels':months,
                'consum':consum

        }


        return Response(data)


class ConsumDataEvents(APIView):
    authentication_classes=[]
    permission_classes = [AllowAny]
    serializer_class = ConsumSerializer

    def get(self,request,string, format=None):
        events=[]
        consum=[]
        event_mnth=Event.objects.filter(date__month=string)
        for event in event_mnth:
            events.append(event.name)
            consum.append(event.total_personal_consum)

        data = {
                "labels":events,
                "consum":consum,
        }
        return Response(data)





###############END DATA-VIEWS#########





############REST_FRAMEWORK#################

class EventRESTView(APIView):
    permission_classes = [AllowAny]
    serializer_class = EventSerializer

    def get(self,request,string):
        user_events = []
        user = User.objects.get(username=string)
        for event in Event.objects.all():
            for role in event.rolemanager_set.all():
                if role.user == user:
                    user_events.append(event)
                    serializer = EventSerializer(user_events, many=True)
        return Response(serializer.data,status=HTTP_200_OK)







#########end REST_FRAMEWORK###############

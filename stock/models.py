from django.db import models
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save, post_init
from django.core.validators import RegexValidator
from django.dispatch import receiver
from django.contrib.auth.models import User, Group
from django import forms
from colorful.fields import RGBColorField



from datetime import date
import datetime
import decimal


class UserProfile(models.Model):
    user = models.OneToOneField(User)





class Stock(models.Model):
    name = models.CharField(max_length=250)
    date_of_creation = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name


    def getDateOfCreation(self):
        return self.date_of_creation

    def getName(self):
        return self.name

    def get_absolute_url(self):
        return reverse('stock:details',kwargs={'pk':self.pk})



class Item(models.Model):
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    on_stock = models.IntegerField(null=True,blank=True)

    unit_over=models.DecimalField(max_digits=5,decimal_places=2,default=0.0)
    #on_store = models.IntegerField()
    on_stock_target = models.IntegerField(null=True,blank=True)
    #   on_store_target = models.IntegerField()
    on_stock_min = models.IntegerField(null=True,blank=True)
    to_order=models.IntegerField(default=0)
    filled=models.IntegerField(default=0)
    price=models.DecimalField(max_digits=5,decimal_places=2,default=0.0)


    item_per_st = models.IntegerField(null=True,blank=True)
    #on_store_min = models.IntegerField()
    TYPE =(
        ('WW','Weisswein'),
        ('RW','Rotwein'),
        ('SP','Spirituosen'),
        ('BR','Bier'),
        ('BRF','Bierflasche'),
        ('AFG','Alkoholfrei'),
        ('MIX','Zum mixen'),
    )
    type=models.CharField(max_length=3,choices=TYPE)

    SIZE = (
        ('1','1L'),
        ('33','0,33L'),
        ('4','0,4L'),
        ('5','0,5L'),
        ('75','0,75L'),
        ('25','0,25'),
        ('7','0,7'),
        ('15','1,5'),
        ('375','0,375')

    )

    size=models.CharField(max_length=3,choices=SIZE )
    def __str__(self):
        return self.name+"-->"+self.stock.name

    def get_absolute_url(self):
        return reverse('stock:details', args=(self.stock.pk,))

    def first_letter(self):
        return self.name and self.name[0] or ''


    @property
    def toOrder(self):
        self.to_order=self.on_stock_target-self.on_stock
        self.save()
        return self.to_order


    def substractOnStockChange(self,c_to_substract,u_to_substract):
        if u_to_substract > self.unit_over:
            self.on_stock=self.on_stock-(c_to_substract+1)
            self.unit_over=self.item_per_st-(u_to_substract-self.unit_over)
            self.save()

        else :
            self.unit_over=self.unit_over-u_to_substract
            self.on_stock=self.on_stock-c_to_substract
            self.save()






#<-------Store------>


class StoreManager(models.Model):
    item = models.ForeignKey('Item')
    name = models.CharField(max_length=250)
    store = models.ForeignKey('Store')
    on_store = models.DecimalField(max_digits=5,decimal_places=2,default=0.0)
    on_store_target = models.IntegerField(default=0)
    toFill = models.IntegerField(default=0)
    #filled = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    def get_on_store(self):
        return self.on_store


    def set_on_store(self,onStore):
        self.on_store= onStore


    def to_fill(self):
        self.toFill=self.on_store_target-self.on_store
        return self.toFill

    #def total_filled(self):
        #self.filled

class Store(models.Model):
    name = models.CharField(max_length=250)
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    user = models.ForeignKey(User,null=True,blank=True)
    items = models.ManyToManyField(Item, through=StoreManager)
    color = RGBColorField(default='#FF0000')
    def __str__(self):
        return self.name





    def itemCnt(self):
        cnt=0
        for storeman in self.storemanager_set.all():
            cnt=cnt+storeman.on_store
        return cnt



    def itemOnStoreFilter(self):
        filtered=[]
        for strmn in self.storemanager_set.all():
            if strmn.on_store!=0:
                filtered.append(strmn.item)
        return filtered


        #filtered=[]
        #for item in self.stock.item_set.all():
        #    exist=False
        #    for itemOnStock in self.items.all():
        #        if(item == itemOnStock):
        #            exist=True
        #            continue
        #    if exist==False:
        #        filtered.append(item)
#
#        return filtered


#<----------Fill Object------>

@receiver(post_save, sender=Item, dispatch_uid="create_storemanager")
def update_store(sender,instance,created,**kwargs):
    if created:
        for store in instance.stock.store_set.all():
            storemanager=StoreManager.objects.create(item=instance,name=instance.name+" "+store.name,store=store,on_store=0,on_store_target=0)


class FillObject(models.Model):
    user = models.ForeignKey(User)
    event = models.ForeignKey('Event',on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username+'-->'+self.event.name


class FillManager(models.Model):
    item = models.ForeignKey('Item')
    name = models.CharField(max_length=250)
    store = models.ForeignKey(Store)
    fillobject = models.ForeignKey(FillObject,blank=True,null=True,on_delete=models.CASCADE)

    CONT = (
        (0,'0'),
        (1,'1'),
        (2,'2'),
        (3,'3'),
        (4,'4'),
        (5,'5'),
        (6,'6'),
        (7,'7'),
        (8,'8'),
    )
    container = models.IntegerField(default=0,choices=CONT)
    unit = models.IntegerField(default=0,blank=True,null=True)
    to_fill = models.IntegerField(default=0)

    def __str__(self):
        return self.name


    def get_absolute_url(self):
        return reverse('stock:store-detail-log', args=(self.store.pk,self.fillobject.pk))


class Delivery(models.Model):
    delivery_date=models.DateField(auto_now_add=True)
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)

    def __str__(self):
        return self.delivery_date.strftime("%Y-%m-%d")+" "+self.stock.name


class DeliveryManager(models.Model):
    delivery=models.ForeignKey(Delivery,on_delete=models.CASCADE)
    item =models.ForeignKey(Item)
    delivered= models.IntegerField()

    def __str__(self):
        return self.item.name+"---> "+str(self.delivery)

    def get_absolute_url(self):
        return reverse('stock:details', args=(self.item.stock.pk,))








class Supplier(models.Model):
    name = models.CharField(max_length=250)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message='Tel. Nummer Format: +99999...',code='invalid_phonenumber')
    phone_number = models.CharField(max_length=15, validators=[phone_regex,]) # validators should be a list
    email = models.EmailField(max_length=250)

    def __str__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(max_length=250)
    date = models.DateField(default=datetime.date.today)
    #date_end = models.DateField(default=datetime.date.today)


    color = RGBColorField(default='#FF0000')

    begin = models.BooleanField(default=False)
    end = models.BooleanField(default=False)
    total_personal_consum = models.DecimalField(max_digits=5,decimal_places=2,default=0.0)

    def __str__(self):
        return self.name



    def get_absolute_url(self):
        return reverse('stock:calender')





    @property
    def event_is_past(self):
        return self.end


    @property
    def event_began(self):
        if self.begin and not self.end:
            return True
        elif self.begin and self.end:
            return False



    @property
    def calc_total_personal_sum(self):
        for consum in self.consum_set.all():
            self.total_personal_consum=self.total_personal_consum+consum.sum
        return self.total_personal_consum



class EventLog(models.Model):
    event=models.ForeignKey(Event,on_delete=models.CASCADE)
    item = models.ForeignKey(Item)
    c_filled = models.IntegerField(null=True,blank=True)
    u_filled = models.IntegerField(null=True,blank=True)
    sales = models.DecimalField(max_digits=11,decimal_places=2,default=0.0)

    def norm_unit_to_container(self):
        div=self.u_filled//self.item.item_per_st
        print(div)
        if div != 0:
            self.c_filled=self.c_filled+div
            self.u_filled=self.u_filled % self.item.item_per_st

    def calc_sales(self):
        self.sales=decimal.Decimal(self.c_filled*self.item.item_per_st)*self.item.price+decimal.Decimal(self.u_filled)*self.item.price

    def __str__(self):
        return self.item.name+'-->'+self.event.name







class RoleManager(models.Model):
    ROLE = (
        ('CVD','CVD'),
        ('CVD_LOG','CVD & Logistik'),

        ('THK','Theke'),
        ('LOG','Logistik'),
        ('GRD','Garderobe'),
        ('CSS','Kasse'),
        ('DJ','dj')
    )
    role=models.CharField(max_length=10,choices=ROLE,blank=True,null=True)
    store = models.ForeignKey(Store,blank=True,null=True)
    user=models.ForeignKey(User,blank=True,null=True)
    event=models.ForeignKey(Event, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username+'-->'+self.event.name

class Consum(models.Model):
    user=models.ForeignKey(User)
    role=models.CharField(max_length=10,null=True,blank=True)
    event=models.ForeignKey(Event,on_delete=models.CASCADE)
    sum=models.DecimalField(max_digits=5,decimal_places=2,default=0.0)

    def __str__(self):
        return self.user.username+'-->'+self.event.name

    def increment_sum(self,incr_price):
        self.sum=self.sum+incr_price

class consumManager(models.Model):
    consum=models.ForeignKey('Consum',on_delete=models.CASCADE)
    storeitem =models.ForeignKey('StoreItem',null=True)
    on_consum=models.IntegerField(default=0)

    def __str__(self):
        return self.storeitem.name

    def increment_consum(self,incr):
        self.on_consum=self.on_consum+incr





class Menu(models.Model):
    name=models.CharField(max_length=255)

    def __str__(self):
        return self.name




class StoreItem(models.Model):
    menu=models.ForeignKey(Menu,on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=5,decimal_places=2)
    TYPE =(
        ('LD','Long drinks'),
        ('KRZ','Kurze'),
        ('AFF','AFG-Flaschen'),
        ('BRF','Bierflasche'),
        ('AFG','Alkoholfrei'),
        ('FLS','FLaschen'),
        ('DZ','Dose'),
        ('SKT','Sekt'),
    )
    type=models.CharField(max_length=3,choices=TYPE,null=True)

    def __str__(self):
        return self.name


    def getMenu(self):
        return self.menu




class JournalEntry(models.Model):
    user=models.ForeignKey(User)
    subject=models.CharField(max_length=255,default="Schichtbeschreibung",null=True)
    text=models.TextField()
    sent_at=models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-sent_at']

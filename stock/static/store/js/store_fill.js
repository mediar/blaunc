$(function () {

  $(".js-store-fill").click(function () {
    $.ajax({
      url: '/store/store/fillitem',
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-storefill").modal("show");
      },
      success: function (data) {
        $("#modal-storefill .modal-content").html(data.html_form);
      }
    });
  });

});



$("#modal-storefill").on("submit", ".js-store-fill-form", function () {
  var form = $(this);
  $.ajax({
    url: form.attr("action"),
    data: form.serialize(),
    type: form.attr("method"),
    dataType: 'json',
    success: function (data) {
      if (data.form_is_valid) {
              window.location = "/store"// <-- This is just a placeholder for now for testing
        $("#modal-storefill").modal("hide");

      }
      else {
        $("#modal-storefill .modal-content").html(data.html_form);
      }
    }
  });
  return false;
});

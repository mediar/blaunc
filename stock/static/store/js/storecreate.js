$(function () {

  $(".js-store-create").click(function () {
    $.ajax({
      url: 'store/create',
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-storecreate").modal("show");
      },
      success: function (data) {
        $("#modal-storecreate .modal-content").html(data.html_form);
      }
    });
  });

});



$("#modal-storecreate").on("submit", ".js-store-create-form", function () {
  var form = $(this);
  $.ajax({
    url: form.attr("action"),
    data: form.serialize(),
    type: form.attr("method"),
    dataType: 'json',
    success: function (data) {
      if (data.form_is_valid) {
              window.location = "/store"// <-- This is just a placeholder for now for testing
        $("#modal-storecreate").modal("hide");

      }
      else {
        $("#modal-storecreate .modal-content").html(data.html_form);
      }
    }
  });
  return false;
});

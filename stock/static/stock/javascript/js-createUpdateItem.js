$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-item-create").modal("show");
      },
      success: function (data) {
        $("#modal-item-create .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          window.location.reload()
          $("#modal-item-create").modal("hide");
        }
        else {
          $("#modal-item-create .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create item
  $(".js-create-item").click(loadForm);
  $("#modal-item-create").on("submit", ".js-create-item-form", saveForm);

  // Update iitem
  $(".js-store-fill").click(loadForm);
  $("#modal-item-create").on("submit", ".js-store-fill-form", saveForm);

});

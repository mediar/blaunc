$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-storeFill").modal("show");
      },
      success: function (data) {
        $("#modal-storeFill .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.formset_is_valid) {
          window.location.reload()
          $("#modal-storeFill").modal("hide");
        }
        else {
          $("#modal-storeFill .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create book
  $(".js-create-book").click(loadForm);
  $("#modal-storefill").on("submit", ".js-book-create-form", saveForm);

  // Update book
  $(".js-store-fill").click(loadForm);
  $("#modal-storefill").on("submit", ".js-store-fill-form", saveForm);

});

$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-item").modal("show");

      },
      success: function (data) {
        $("#modal-item .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          window.location.reload()
          $("#modal-item").modal("hide");


        }
        else {
          $("#modal-item .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };




  // Create item
  $(".js-create-item").click(loadForm);
  $("#modal-item").on("submit", ".js-item-create-form", saveForm);


});

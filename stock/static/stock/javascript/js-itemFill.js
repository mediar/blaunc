$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-itemFill").modal("show");
      },
      success: function (data) {
        $("#modal-itemFill .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          window.location.reload()
          $("#modal-itemFill").modal("hide");
        }
        else {
          $("#modal-itemFill .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create book
  $(".js-create-book").click(loadForm);
  $("#modal-itemFill").on("submit", ".js-book-create-form", saveForm);

  // Update item
  $(".js-item-fill").click(loadForm);
  $("#modal-itemFill").on("submit", ".js-item-fill-form", saveForm);

});

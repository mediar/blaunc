$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-itemToStoreFill").modal("show");
      },
      success: function (data) {
        $("#modal-itemToStoreFill .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          window.location.reload()
          $("#modal-itemToStoreFill").modal("hide");
        }
        else {
          $("#modal-itemToStoreFill .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create book
  $(".js-create-book").click(loadForm);
  $("#modal-storefill").on("submit", ".js-book-create-form", saveForm);

  // Update book
  $(".js-itemToStore-fill").click(loadForm);
  $("#modal-itemToStoreFill").on("submit", ".js-itemToStore-fill-form", saveForm);

});

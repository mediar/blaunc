$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-role").modal("show");

      },
      success: function (data) {
        $("#modal-role .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          window.location.reload()
          $("#modal-role").modal("hide");
        }
        else {
          $("#modal-role .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };


  /* Binding */

  // Create role
  $(".js-create-role").click(loadForm);
  $("#modal-role").on("submit", ".js-role-create-form", saveForm);



});

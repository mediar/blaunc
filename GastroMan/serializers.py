from django.contrib.auth.models import User, Group
from rest_framework import serializers, viewsets
from django.core.exceptions import ValidationError



class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('username',)

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)


class UserLoginSerializer(serializers.ModelSerializer):
    username = serializers.CharField()
    class Meta:
        model = User
        fields = ('id','username','password')
        extra_kwargs = {"password":{"write_only": True}}

    def validate(self,data):
        user_obj=None
        username = data.get("username",None)
        password = data["password"]
        if not username:
            raise ValidationError("a username is required")
        user = User.objects.filter(username=username)
        if user.exists():
            user_obj=user.first()
            if not user_obj.check_password(password):
                raise ValidationError("incorrect password")

        else:
            raise ValidationError("this username does not exist")
        return data

from django.http import HttpResponse
from django.http import Http404
from django.shortcuts import render, get_object_or_404,redirect, redirect
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.views import generic
from django.views.generic import View, TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from .forms import UserForm, UserLoginForm
from stock.models import Event
from django.contrib import messages
from django.contrib.auth.forms import PasswordChangeForm

from rest_framework.permissions import AllowAny
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import UserLoginSerializer



from datetime import date



class IndexView(generic.TemplateView):

    template_name="base.html"
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        try:
            context['event_now'] = Event.objects.get(date=date.today())
            context['no_event'] = False
        except Event.DoesNotExist:
            context['no_event'] = True
        return context



class UserFormView(View):
    form_class = UserForm
    template_name = 'registrationform.html'

    def get(self,request):
        form = self.form_class(None)
        return render(request,self.template_name,{'form':form})

    def post(self,request):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = form.save(commit=False)

            #Cleaned (Normalized) Data
            username=form.cleaned_data['username']
            password=form.cleaned_data['password']
            user.set_password(password)
            user.save()


            user = authenticate(username=username,password=password)

            if user is not None:

                if user.is_active:
                    login(request,user)
                    return redirect('home')

        return render(request,self.template_name,{'form':form})



class UserLoginRESTView(APIView):
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer

    def post(self,request):
        data = request.data
        print("got post request")
        serializer = UserLoginSerializer(data =data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors,status=HTTP_400_BAD_REQUEST)





class LoginFormView(View):
    form_class = UserLoginForm
    template_name = 'loginform.html'
    def get(self,request):
        form = self.form_class(None)
        return render(request,self.template_name,{'form':form})
    def post(self,request):
        form = self.form_class(request.POST)
        if form.is_valid():


            #Cleaned (Normalized) Data
            username=form.cleaned_data['username']
            password=form.cleaned_data['password']



            user = authenticate(username=username,password=password)

            if user is not None:

                if user.is_active:
                    login(request,user)
                    return redirect('home')

        return render(request,self.template_name,{'form':form})


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('logout')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'change_password.html', {
        'form': form
    })

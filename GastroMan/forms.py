from django.contrib.auth.models import User
from django import forms




class UserForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput)
    email = forms.CharField(widget=forms.EmailInput)


    class Meta:
        model = User
        fields = ['username' , 'email', 'password',]


class UserLoginForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    username = forms.CharField()

    class Meta:
        model = User
        fields = ['username' ,'password',]
